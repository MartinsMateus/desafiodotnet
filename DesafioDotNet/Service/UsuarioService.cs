﻿using DesafioDotNet.Data;
using DesafioDotNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioDotNet.Service
{
    public class UsuarioService
    {
        private readonly ApplicationDbContext _context;

        public UsuarioService (ApplicationDbContext context)
        {
            _context = context;
        }

        public Usuario Get(Guid usuarioUid)
        {
            if (usuarioUid != null)
            {
                return _context.User.Find(usuarioUid);
            }
            else
            {
                return null;
            }
            
        }

        public void Create(Usuario usuario)
        {
            _context.User.Add(usuario);          
            _context.SaveChanges();
        }

        public Usuario GetByEmail(string email)
        {
            if (!String.IsNullOrEmpty(email))
            {
                return _context.User.Where(x => x.email == email).FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public List<Phones> GetPhones(Guid usuarioUid)
        {
            if (usuarioUid != null)
            {
                return _context.Phone.Where(x => x.UsersId == usuarioUid).ToList();
            }
            return null;
        }

        public void Update(Usuario usuario)
        {
            _context.User.Update(usuario);
            _context.SaveChanges();
        }
    }
}
