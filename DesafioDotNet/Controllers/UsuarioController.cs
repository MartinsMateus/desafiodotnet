﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DesafioDotNet.Models;
using DesafioDotNet.Service;
using DesafioDotNet.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DesafioDotNet.Controllers
{
    [Route("api/[controller]")]
    [Authorize()]
    public class UsuarioController : Controller
    {
        private readonly UsuarioService _service;
        private readonly IConfiguration _configuration;

        public UsuarioController(UsuarioService service, IConfiguration configuration)
        {
            _service = service;
            _configuration = configuration;
        }

        [HttpGet("me")]
        public ActionResult Get()
        {
            if (ModelState.IsValid)
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;

                // Gets list of claims.
                IEnumerable<Claim> claim = identity.Claims;
                List<String> userName = new List<string>();

                foreach (var item in claim)
                {
                    userName.Add(item.Value);
                }



                var user = _service.GetByEmail(userName.FirstOrDefault());

                if (user != null)
                {
                    var phones = _service.GetPhones(user.usuarioUid);

                    foreach (var item in phones)
                    {
                        user.phones.Add(item);
                    }

                    return Ok(new { user });
                }           
            }

            return BadRequest(ModelState);
            
        }

        [AllowAnonymous]
        [HttpPost("signup")]
        public ActionResult Register([FromBody] RegisterViewModel model)
        {
           
            if (ModelState.IsValid)
            {               
                var phoneList = model.phoneList;
                
                var usuario = new Usuario { usuarioUid = Guid.NewGuid(), firstName = model.FirstName, lastName = model.LastName, email = model.Email, password = model.Password, phones = phoneList, created_at = DateTime.Now, last_login =  DateTime.MinValue};                
                var hasUsuario = _service.GetByEmail(usuario.email);            
                
                if (hasUsuario == null)
                {
                    try
                    {
                        _service.Create(usuario);
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(ex.Message);
                    }
                    
                    return Ok(
                        new
                        {
                            Mensagem = "",
                            usuario

                        });
                }
                else
                {
                    return NotFound(
                      new
                      {
                          Mensagem = "E-mail already exists",
                          Erro = true
                      });
                }
            }


            return BadRequest(ModelState);
        }

        [AllowAnonymous]
        [HttpPost("signin")]
        public ActionResult Login([FromBody] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _service.GetByEmail(model.email);
                user.last_login = DateTime.Now;
                _service.Update(user);
                var phones = _service.GetPhones(user.usuarioUid);

                foreach (var item in phones)
                {
                    user.phones.Add(item);
                }
                if (user != null)
                {

                    #region criar token                                
                    //symmetric security key
                    var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["SecurityKey"]));

                    //signing credentials
                    var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

                    //add claims
                    var claims = new List<Claim>
                    {
                        new Claim("email", model.email)
                    };


                    //create token
                    var token = new JwtSecurityToken(
                           issuer: "desafio.dot.net",
                           audience: "desafio.dot.net",
                           expires: DateTime.Now.AddHours(1),
                           signingCredentials: signingCredentials
                           , claims: claims
                       );

                    var teste = new JwtSecurityTokenHandler().WriteToken(token);

                    #endregion

                    return Ok(
                        new
                        {
                            Mensagem = "Sucesso ao logar",                            
                            token = new JwtSecurityTokenHandler().WriteToken(token)
                        });
                }
                else
                {
                    return NotFound(
                       new
                       {
                           Mensagem = "Invalid e-mail or password",
                           Erro = true
                       });
                }
            }

            return BadRequest(ModelState);
        }


    }
}
