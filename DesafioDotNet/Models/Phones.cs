﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioDotNet.Models
{
    public class Phones
    {
        public Int32 phoneid { get; set; }
        public Int32 number { get; set; }
        public Int32 area_code { get; set; }
        public String country_code { get; set; }
        public Usuario usuario { get; set; }
        public virtual Guid UsersId { get; set; }
    }
}
