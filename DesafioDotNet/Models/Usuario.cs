﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioDotNet.Models
{
    public class Usuario
    {
        public Guid usuarioUid { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public String email { get; set; }
        public String password { get; set; }
        public virtual ICollection<Phones> phones { get; set; }
        public DateTime created_at { get; set; }
        public DateTime last_login { get; set; }

    }
}
