﻿using DesafioDotNet.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioDotNet.ViewModel
{    
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Missing fields")]  
        [Display(Name = "Nome")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Missing fields")]
        [Display(Name = "Sobrenome")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Missing fields")]
        [EmailAddress(ErrorMessage = "Invalid fields")]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Missing fields")]
        [DataType(DataType.Password, ErrorMessage = "Invalid fields")]
        [Display(Name = "Senha")]
        public string Password { get; set; }
        
        public List<Phones> phoneList { get; set; }
    }
}
