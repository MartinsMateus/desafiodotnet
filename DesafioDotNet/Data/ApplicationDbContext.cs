﻿using DesafioDotNet.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioDotNet.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) :
            base(options)
        { }

        public DbSet<Usuario> User { get; set; }
        public DbSet<Phones> Phone { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>()
                .HasKey(p => p.usuarioUid);
            modelBuilder.Entity<Phones>()
                .HasKey(p => p.phoneid);

            modelBuilder.Entity<Usuario>()
             .HasMany(c => c.phones)
             .WithOne(e => e.usuario).HasForeignKey(a => a.number);

            modelBuilder.Entity<Usuario>()
               .HasMany(e => e.phones)
               .WithOne(x => x.usuario).HasForeignKey(b => b.UsersId)
               .IsRequired();
        }

    }
}
